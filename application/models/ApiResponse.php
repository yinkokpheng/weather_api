<?php

/**
 * @SWG\Definition(type="object")
 */
class ApiResponse
{

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    public $code = 200;

    /**
     * @SWG\Property
     * @var bool
     */
    public $status = true;

    /**
     * @SWG\Property
     * @var string
     */
    public $message;

    /**
     * @SWG\Property
     * @var object
     */
    public $data;
}
