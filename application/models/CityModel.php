<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CityModel extends CI_Model
{

    var $table = 'cities';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        if (!is_null($id)) {
            $this->db->where('id', $id);
            $query = $this->db->get($this->table);
            if ($query->num_rows() === 1) {
                return $query->row_array();
            }

            return null;
        }

        $this->db->order_by('id','desc');
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return null;
    }

    public function get_current_page_records($limit, $page)
    {
        $this->db->limit($limit, (($page - 1) * $limit ));
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            $pagination = array();
            $pagination['page'] = (int)$page;
            $pagination['limit'] = (int)$limit;
            $pagination['total_pages'] = ceil($this->get_total() / $limit);
            $pagination['total_count'] = $this->get_total();

            $result = array();
            $result['cities'] =  $query->result_array();
            $result['pagination'] = $pagination;

            return $result;
        }

        return null;
    }

    public function get_total()
    {
        return $this->db->count_all($this->table);
    }

    public function save($city)
    {
        if ($city !== NULL) {
            $this->db->insert($this->table, $city);

            if ($this->db->affected_rows() === 1) {
                return $this->db->insert_id();
            }
        }
        return null;
    }

    public function update($city)
    {
        if ($city !== NULL) {
            $id = $city->id;

            $this->db->where('id', $id);
            $this->db->update($this->table, $city);

            if ($this->db->affected_rows() === 1) {
                return true;
            }
        }

        return null;
    }

    public function delete($id)
    {
        $city = $this->db->where('id', $id)->get($this->table);

        if ($city !== NULL) {
            $this->db->where('id', $id)->delete($this->table);

            if ($this->db->affected_rows() === 1) {
                return true;
            }
        }
        return null;
    }
}