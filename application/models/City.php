<?php
/**
 * Created by PhpStorm.
 * User: kokpheng
 * Date: 23-Dec-17
 * Time: 03:15 PM
 */

/**
 * @SWG\Definition(required={"name"}, type="object", @SWG\Xml(name="City"))
 */
class City
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Phnom Penh")
     * @var string
     */
    public $name;
}
