<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForecastModel extends CI_Model
{
    var $table = 'forecast';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id_city = null)
    {
        if (!is_null($id_city)) {
            $this->db->select('forecast.id, forecast.forecast, forecast.date, forecast.id_city, cities.name')->from($this->table)->where('id_city', $id_city)->join('cities', 'cities.id = forecast.id_city')->order_by('date', 'ASC');
            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                $row =  $query->row();
                $forecast = array();

                $forecast['id'] = $row->id;
                $forecast['forecast'] = $row->forecast;
                $forecast['date'] = $row->date;

                $city = array();
                $city['id'] = $row->id_city;
                $city['name'] = $row->name;
                $forecast['city'] = $city;

                return $forecast;
            }

            return null;
        }

        $this->db->select('forecast.id, forecast.forecast, forecast.date, forecast.id_city, cities.name');
        $this->db->from($this->table);
        $this->db->join('cities', 'cities.id = forecast.id_city');
        $query = $this->db->get();

        $rows = $query->result_array();
        $forecasts = array();
        foreach ($rows as $row)
        {
            $forecast = array();

            $forecast['id'] = $row["id"];
            $forecast['forecast'] = $row["forecast"];
            $forecast['date'] = $row["date"];

            $city = array();
            $city['id'] = $row["id_city"];
            $city['name'] = $row["name"];
            $forecast['city'] = $city;

            array_push($forecasts, $forecast);
        }

        if ($query->num_rows() > 0) {
            return $forecasts;
        }

        return null;
    }

    public function save($forecast)
    {
        if ($forecast !== NULL) {
            $data = array(
                'id' => $forecast->city->id,
                'forecast' => $forecast->forecast,
                'id_city' => $forecast->city->id,
                'date' => date('Y-m-d H:i:s')
            );

            $this->db->insert($this->table, $data);

            if ($this->db->affected_rows() === 1) {
                return $this->db->insert_id();
            }
        }
        return null;
    }

    public function update($id, $forecast)
    {
        if ($forecast !== NULL) {
            $data = array(
                'forecast' => $forecast->forecast,
                'id_city' => $forecast->city->id,
                'date' => date('Y-m-d H:i:s')
            );

            $this->db->where('id', $id);
            $this->db->update($this->table, $data);

            if ($this->db->affected_rows() === 1) {
                return true;
            }
        }

        return null;
    }

    public function delete($id)
    {
        $forecast = $this->db->where('id', $id)->get($this->table);

        if ($forecast !== NULL) {
            $this->db->where('id', $id)->delete($this->table);

            if ($this->db->affected_rows() === 1) {
                return true;
            }
        }
        return false;
    }
}