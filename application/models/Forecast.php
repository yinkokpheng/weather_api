<?php
/**
 * Created by PhpStorm.
 * User: kokpheng
 * Date: 23-Dec-17
 * Time: 03:17 PM
 */

/**
 * @SWG\Definition(required={"forecast", "city"}, type="object", @SWG\Xml(name="Forecast"))
 */
class Forecast
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="Phnom Penh Forecast")
     * @var string
     */
    public $forecast;

    /**
     * @var City
     * @SWG\Property()
     */
    public $city;
}
