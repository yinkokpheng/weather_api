<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'city';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

// Routes for city
$route['city']['get'] = 'CityController/index'; //www.example.com/city
$route['city/(:num)']['get'] = 'CityController/find/$1'; //www.example.com/city/1
$route['city']['post'] = 'CityController/index'; //www.example.com/city
$route['city/(:num)']['put'] = 'CityController/index/$1'; //www.example.com/city/1
$route['city/(:num)']['delete'] = 'CityController/index/$1'; //www.example.com/city/1

// Routes for forecast
$route['forecast']['get'] = 'ForecastController/index';
$route['forecast/(:num)']['get'] = 'ForecastController/find/$1';
$route['forecast']['post'] = 'ForecastController/index';
$route['forecast/(:num)']['put'] = 'ForecastController/index/$1';
$route['forecast/(:num)']['delete'] = 'ForecastController/index/$1';

// Routes for file
$route['file_upload']['post'] = 'FileController/index';

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
