<?php
/**
 * @SWG\SecurityScheme(
 *   securityDefinition="basicAuth",
 *   type="basic",
 *   in="header",
 *   name="Authorization"
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="apiKey",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="petstore_auth",
 *   type="oauth2",
 *   authorizationUrl="http://petstore.swagger.io/oauth/dialog",
 *   flow="implicit",
 *   scopes={
 *     "read:pets": "read your pets",
 *     "write:pets": "modify pets in your account"
 *   }
 * )
 */
