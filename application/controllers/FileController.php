<?php
/**
 * Created by PhpStorm.
 * User: kokpheng
 * Date: 1/17/18
 * Time: 8:37 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class FileController extends REST_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        $this->load->helper(array('form', 'url'));
    }

    /**
     * @SWG\Post(
     *     path="/file_upload",
     *     tags={"file"},
     *     operationId="uploadFile",
     *     summary="Upload multiple files",
     *     description="",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="userfile",
     *         in="formData",
     *         description="file to upload",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     */
    public function index_post()
    {
        $config['upload_path']          = './uploads/'; // You have to create this folder in your project path and give read/write permission
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 0; // default 0
        $config['max_width']            = 0; // default 0
        $config['max_height']           = 0; // default 0
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        $response = new ApiResponse();

        if ( ! $this->upload->do_upload('userfile')) // userfile : request paramter of uploaded file
        {
            $error_message = strip_tags($this->upload->display_errors()); // strip_tags: remove html tage
            $response->message = "Upload image error: " . $error_message;
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }
        else
        {
            $image_info = $this->upload->data();
            $image_info['image_path'] = base_url() . 'uploads/' . $image_info['file_name']; // Add new image path
            $response->message = "Upload image success";
            $response->data = $image_info;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }
    }
}