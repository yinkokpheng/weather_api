<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class CityController extends REST_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['index_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /**
     * @SWG\Get(
     *     path="/city",
     *     summary="Get all cities",
     *     description="Multiple status values can be provided with comma separated strings",
     *     operationId="getAllCities",
     *     produces={"application/json", "application/xml"},
     *     tags={"city"},
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number (Offset - Optional)",
     *     type="integer",
     *     format="int64",
     *     default=1
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit amount of record (Optional)",
     *     type="integer",
     *     format="int64",
     *     default=10
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/City")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid status value",
     *     ),
     *     security={
     *       {"apiKey": {}}
     *     }
     * )
     */
    public function index_get()
    {
        $limit = $this->get('limit');
        $offset = $this->get('page');

        if (!$limit && !$offset) {
            $cities = $this->CityModel->get();

            $response = new ApiResponse();

            // If the cities parameter exist return all the cities
            if (!is_null($cities)) {
                // Set the response and exit
                $response->message = "Cities were found";
                $response->data = $cities;
                $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $response->message = "No cities were found";
                $response->code = REST_Controller::HTTP_NOT_FOUND;
                $response->status = FALSE;
                $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }else {
            $result = $this->CityModel->get_current_page_records(($limit ?? 10), ($offset ?? 1));

            $response = new ApiResponsePagination();

            // If the cities parameter exist return all the cities
            if (!is_null($result)) {
                // Set the response and exit
                $response->message = "Cities were found";
                $response->data = $result['cities'];
                $response->pagination = $result['pagination'];
                $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $response->message = "No cities were found";
                $response->code = REST_Controller::HTTP_NOT_FOUND;
                $response->status = FALSE;
                $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    /**
     * @SWG\Get(
     *     path="/city/{id}",
     *     summary="Find city by ID",
     *     description="Returns a single city",
     *     operationId="getCityById",
     *     tags={"city"},
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         description="ID of city to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/City")
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="City not found"
     *     ),
     *     security={
     *       {"basicAuth": {}}
     *     }
     * )
     */
    public function find_get($id)
    {
        $response = new ApiResponse();

        // Find and return a single record for a particular city.
        if (!$id) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        // Get the city from the database, using the id as key.
        // Usually a model is to be used for this.
        $city = $this->CityModel->get($id);

        if (!is_null($city)) {
            $response->message = "City was found";
            $response->data = $city;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "City could not be found";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    /**
     * @SWG\Post(
     *     path="/city",
     *     tags={"city"},
     *     operationId="addCity",
     *     summary="Add a new city",
     *     description="",
     *     consumes={"application/json", "application/xml"},
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="CIty object that needs to be added to the store",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/City"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     */
    public function index_post()
    {
        $response = new ApiResponse();
        // If the cities parameter doesn't exist return null
        if (!$this->post('name')) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $city = new City; // Load Model: $this->load->model('City');
        $city->name = $this->post('name');

        $id = $this->CityModel->save($city);

        if (!is_null($id)) {
            $response->message = "City was added and id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "City could not be added";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Put(
     *   path="/city/{id}",
     *   tags={"city"},
     *   summary="Updated city",
     *   description="This can only be done by city id",
     *   operationId="updateCity",
     *   produces={"application/json", "application/xml"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id of city that need to be updated",
     *     required=true,
     *     type="integer",
     *     format="int64"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Updated user object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/City")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *   ),
     *   @SWG\Response(response=400, description="Invalid city supplied"),
     *   @SWG\Response(response=404, description="City not found")
     * )
     */
    public function index_put($id)
    {
        $response = new ApiResponse();
        // If the cities parameter doesn't exist return null
        if (!$id || !$this->put('name')) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $city = new City;
        $city->id = $id;
        $city->name = $this->put('name');

        $update = $this->CityModel->update($city);

        if (!is_null($update)) {
            $response->message = "City was updated by id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "City could not be updated";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Delete(
     *     path="/city/{id}",
     *     tags={"city"},
     *     summary="Deletes a city",
     *     description="",
     *     operationId="deleteCity",
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         description="City id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="City not found"
     *     )
     * )
     */
    public function index_delete($id)
    {
        $response = new ApiResponse();
        // Validate the id.
        if ($id <= 0) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $delete = $this->CityModel->delete($id);

        if (!is_null($delete)) {
            $response->message = "City was deleted by id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "City could not be deleted";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
}