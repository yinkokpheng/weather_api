<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class ForecastController extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @SWG\Get(
     *     path="/forecast",
     *     summary="Get all forecasts",
     *     description="Multiple status values can be provided with comma separated strings",
     *     operationId="getAllForecast",
     *     produces={"application/json", "application/xml"},
     *     tags={"forecast"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Forecast")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid status value",
     *     )
     * )
     */
    public function index_get()
    {
        $forecasts = $this->ForecastModel->get();

        $response = new ApiResponse();

        if (!is_null($forecasts)) {
            // Set the response and exit
            $response->message = "Forecasts were found";
            $response->data = $forecasts;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $response->message = "No forecasts were found";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Get(
     *     path="/forecast/{city_id}",
     *     summary="Find forecast by city ID",
     *     description="Returns a single forecast",
     *     operationId="getForecastByCityId",
     *     tags={"forecast"},
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         description="ID of city to return",
     *         in="path",
     *         name="city_id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Forecast")
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Forecast not found"
     *     )
     * )
     */
    public function find_get($id_city)
    {
        $response = new ApiResponse();

        if (!$id_city) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $forecast = $this->ForecastModel->get($id_city);

        if (!is_null($forecast)) {
            $response->message = "Forecast was found";
            $response->data = $forecast;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "Forecast could not be found";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Post(
     *     path="/forecast",
     *     tags={"forecast"},
     *     operationId="addForecast",
     *     summary="Add a new forecast",
     *     description="",
     *     consumes={"application/json", "application/xml"},
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Forecast object that needs to be added to the store",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/Forecast"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input",
     *     )
     * )
     */
    public function index_post()
    {
        $response = new ApiResponse();
        if (!$this->post('forecast') || !$this->post('city')['id']) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $forecast = new Forecast();
        $forecast->forecast = $this->post('forecast');

        $city = new City;
        $city->id = $this->post('city')['id'];
        $forecast->city = $city;


        $id = $this->ForecastModel->save($forecast);

        if (!is_null($id)) {
            $response->message = "Forecast was added and id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "Forecast could not be added";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Put(
     *   path="/forecast/{id}",
     *   tags={"forecast"},
     *   summary="Updated forecast",
     *   description="This can only be done by forecast id",
     *   operationId="updateCity",
     *   produces={"application/json", "application/xml"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id of forecast that need to be updated",
     *     required=true,
     *     type="integer",
     *     format="int64"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Updated user object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Forecast")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *   ),
     *   @SWG\Response(response=400, description="Invalid city supplied"),
     *   @SWG\Response(response=404, description="City not found")
     * )
     */
    public function index_put($id)
    {
        $response = new ApiResponse();
        if (!$this->put('forecast') || !$id || !$this->put('city')['id']) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $forecast = new Forecast();
        $forecast->id = $this->put('id');
        $forecast->forecast = $this->put('forecast');

        $city = new City;
        $city->id = $this->put('city')['id'];
        $forecast->city = $city;

        $update = $this->ForecastModel->update($id, $forecast);

        if (!is_null($update)) {
            $response->message = "Forecast was updated by id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "City could not be updated";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @SWG\Delete(
     *     path="/forecast/{id}",
     *     tags={"forecast"},
     *     summary="Deletes a forecast",
     *     description="",
     *     operationId="deleteForecast",
     *     produces={"application/json", "application/xml"},
     *     @SWG\Parameter(
     *         description="Forecast id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Forecast not found"
     *     )
     * )
     */
    public function index_delete($id)
    {
        $response = new ApiResponse();
        if (!$id) {
            $response->message = "Invalid paramters";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

        $delete = $this->ForecastModel->delete($id);

        if (!is_null($delete)) {
            $response->message = "Forecast was deleted by id : " . $id;
            $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $response->message = "Forecast could not be deleted";
            $response->code = REST_Controller::HTTP_NOT_FOUND;
            $response->status = FALSE;
            $this->response($response, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
}